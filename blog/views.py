from django.shortcuts import render, redirect
from django.core.mail import EmailMessage, send_mail
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    return render(request, 'main/index.html', {})

# 이메일 전송
def contact_mail(request):
    print(1)
    if request.method == 'POST':
        print(2)
        if request.POST['email'] is None or request.POST['email'] == "":
            return redirect('/')
        try:
            send_mail(request.POST['name'], "SendEmail : " + request.POST['email']+"\n"+request.POST['message'],
                      request.POST.get('email', 'kqpwo123@gmail.com'), ['kqpwo12@naver.com'])
        except Exception as e:
            print('error : ' + str(e))
            return redirect('/')
    return HttpResponseRedirect('/')
