
from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.index, name='index'),
    path('contact_mail/', views.contact_mail, name='contact_mail'),
]
