#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>
#include<time.h>
#include<memory.h>
#pragma warning(disable:4996)

#define max_x 24
#define max_y 24

int M[max_y][max_x]={0,};
char Block[4][4]={". ","●","○","☆"};
char ch, ch1='y';
int x=0, y=0;
int n=0, i=0, k=0;
int cnt1=0, cnt2=0;
int cnt=0;
int mx=0, my=0;
int blackwin=0, whitewin=0;
clock_t seconds=0, seconds1=0, seconds2=0, seconds3=0, seconds4=0;
int Blacktime=0, Whitetime=0;

void gotoxy(int x,int y);
void map(void);
void movekey(void);
void PlayTime(void);
int cond(void);
int handicap(void);

#pragma region main함수
int main(void)
{

	do{
		system("cls");
		x=0, y=0;
		n=0, i=0, k=0;
		cnt=0, cnt1=0, cnt2=0;
		mx=0, my=0;
		seconds=0;
		Blacktime=0;
		Whitetime=0;

		memset(M, 0, sizeof(M));

		map();

		x=12, y=12;
		gotoxy(x*2,y);

		gotoxy(52,5);
		printf("x좌표 = %4d",x);
		gotoxy(52,6);
		printf("y좌표 = %4d",y);
		gotoxy(52,7);
		printf("까만 돌을 놓은 횟수 = %d", cnt1);
		gotoxy(52,8);
		printf("흰 돌을 놓은 횟수 = %d", cnt2);

		gotoxy(x*2,y);

		i=1;
		do{
			seconds1=clock();
			seconds3=clock();

			
			cond();
			movekey();
			handicap();
			//PlayTime();
		}while(cond());
	}while(ch1=='y' || ch1=='Y');
	return 0;
}
#pragma endregion
#pragma region map
void map(void)
{
	for(n=0; n<max_x; n++)
	{
		M[0][n]=3;
		M[max_y-1][n]=3;
	}
	for(i=1; i<max_y-1; i++)
	{
		M[i][0]=3;
		M[i][max_x-1]=3;
	}
	for(n=0; n<max_y; n++)
	{
		for(i=0, k=0; i<max_x; i++, k++)
		{
			gotoxy(x+k*2,y+n);
			printf("%s",Block[M[n][i]]);
		}
	}
}
#pragma endregion 
#pragma region movekey
void movekey(void)
{
	if(kbhit())
	{
		ch=getch();
		if(ch==-32)
		{
			ch=getch();
			switch(ch)
			{
			case 72:
				if(y>1)
				{
					y--;
					gotoxy(52,5);
					printf("x좌표 = %4d",x);
					gotoxy(52,6);
					printf("y좌표 = %4d",y);
				}
				break;
			case 80:
				if(y<max_y-2)
				{
					y++;
					gotoxy(52,5);
					printf("x좌표 = %4d",x);
					gotoxy(52,6);
					printf("y좌표 = %4d",y);
				}
				break;
			case 75:
				if(x>1)
				{
					x--;
					gotoxy(52,5);
					printf("x좌표 = %4d",x);
					gotoxy(52,6);
					printf("y좌표 = %4d",y);
				}
				break;
			case 77:
				if(x<max_x-2)
				{
					x++;
					gotoxy(52,5);
					printf("x좌표 = %4d",x);
					gotoxy(52,6);
					printf("y좌표 = %4d",y);

				}
				break;
			}
			gotoxy(x*2,y);
		}
		else if(ch==32)
		{
			if(i==1)
			{
				if(M[y][x]==0)
				{
					M[y][x]=1;
					printf("%s",Block[i]);
					i=2;
					cnt1++;
				}
				else if(M[y][x]==1)
				{
					Sleep(100);
				}
				else
				{
					gotoxy(52,10);
					printf("그곳엔 다른 돌이 있어요");
					Sleep(500);
					gotoxy(52,10);
					printf("                        ");
				}
				gotoxy(52,7);
				printf("까만 돌을 놓은 횟수 = %d", cnt1);
				gotoxy(x*2,y);


			}

			else if(i==2)
			{
				if(M[y][x]==0)
				{
					M[y][x]=2;
					printf("%s",Block[i]);
					i=1;
					cnt2++;
				}
				else if(M[y][x]==2)
				{
					Sleep(100);
				}
				else
				{
					gotoxy(52,10);
					printf("그곳엔 다른 돌이 있어요");
					Sleep(500);
					gotoxy(52,10);
					printf("                        ");
				}
				gotoxy(52,8);
				printf("흰 돌을 놓은 횟수 = %d", cnt2);
				gotoxy(x*2,y);


			}
		}	
	}

}
#pragma endregion
int cond(void)
{
	/*		세로로 이겼을 때		*/
	for(n=0; n<max_x; n++)
	{
		for(k=0; k<max_y; k++)
		{
			if(M[n][k]==1)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("검정 돌 Win\n");
					blackwin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;

				}
			}
			else
				cnt=0;
		}
	}
	for(n=0; n<max_x; n++)
	{
		for(k=0; k<max_y; k++)
		{
			if(M[n][k]==2)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("흰 돌 Win\n");
					whitewin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}
	/*		가로로 이겼을 때		*/
	for(n=0; n<max_x; n++)
	{
		for(k=0; k<max_y; k++)
		{
			if(M[k][n]==1)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("검정 돌 Win\n");
					blackwin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}


	for(n=0; n<max_x; n++)
	{
		for(k=0; k<max_y; k++)
		{
			if(M[k][n]==2)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("흰 돌 Win\n");
					whitewin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;

				}
			}
			else
				cnt=0;
		}
	}
	/*		 반대 대각선으로 이겼을 때		*/
	for(mx=0; mx<max_x; mx++)
	{
		for(n=0, k=0; n<max_x; n++, k++)
		{
			if(M[k][n+mx]==1)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("검정 돌 Win\n");
					blackwin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}
	for(mx=0; mx<max_x; mx++)
	{
		for(n=0, k=0; n<max_x; n++, k++)
		{
			if(M[k][n+mx]==2)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("흰 돌 Win\n");
					whitewin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}
	/*		빗금모양 대각선 으로 이겻을때 	*/
	for(mx=0; mx<max_x; mx++)
	{
		for(n=0, k=max_y; k>0; n++, k--)
		{
			if(M[k][n+mx]==1)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("검정 돌 Win\n");
					blackwin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}
	for(mx=0; mx<max_x; mx++)
	{
		for(n=0, k=max_y; k>0; n++, k--)
		{
			if(M[k][n+mx]==2)
			{
				cnt++;
				if(cnt==5)
				{
					Sleep(500);
					system("cls");
					gotoxy(30,10);
					printf("흰 돌 Win\n");
					whitewin++;
					gotoxy(30,12);
					printf("다시하시겠습니까?(Y/N)");
					ch1=getch();
					if(ch1=='n' || 'N')
						return 0;
				}
			}
			else
				cnt=0;
		}
	}
	return 1;

}
int handicap(void)
{
	if(i==1 && blackwin>0)
	{
		seconds=clock()/1000;
		if(seconds>15)
		{
			whitewin++;
			system("cls");
			gotoxy(30,10);
			printf("시간 초과, 흰 돌 win");
			gotoxy(30,12);
			printf("다시하시겠습니까?(Y/N)");
			ch1=getch();
			if(ch1=='n' || 'N')
				return 0;
		}
	}
	if(i==2 && whitewin>0)
	{
		seconds=clock()/1000;
		if(seconds>15)
		{
			blackwin++;
			system("cls");
			gotoxy(30,10);
			printf("시간 초과, 검은 돌 win");
			gotoxy(30,12);
			printf("다시하시겠습니까?(Y/N)");
			ch1=getch();
			if(ch1=='n' || 'N')
				return 0;
		}
	}
	return 1;
}
void PlayTime(void)
{
	
	gotoxy(52,10);
	printf("플레이시간 %d초",seconds);
	gotoxy(52,11);
	printf("검은 돌 play시간 %d초", Blacktime);
	gotoxy(52,12);
	printf("흰 돌 play시간 %d초", Whitetime);
	gotoxy(x*2,y);
}
#pragma region gotoxy
void gotoxy(int x, int y)
{
	COORD Cur;
	Cur.X=x;
	Cur.Y=y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),Cur);
}
#pragma endregion